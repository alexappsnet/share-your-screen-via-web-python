# README #

This app allows you to share your Linux or Windows screen via Web. Your colleges or relatives can watch your screen via their Web Browser while you are demonstrating some feature or problem.

See more details on http://alexapps.net/share-your-screen-web/. 

### What is this repository for? ###

This application is a Python program that uses PyQt4 (for Linux) or Pillow (for Windows) to grab your screen and Tornado to serve the client web requests.


### How do I get set up? ###

* Install Python 3
* Install Tornado for Python 3
* Install PyQt4 for Python 3 (for Linux)
* Install Pillow for Python 3 (for Windows)
* Run "run.py"

### Supported Platforms ###

* Linux (tested on Ubuntu 14)
* Windows (tested on Windows 7)
* Now I got it working on Mac OS as well with Quartz compiled (tested on Mac OS 10)