#!/usr/bin/env python3

import io
import os
import time
 
from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application, url
from tornado.gen import coroutine, Task
from tornado.concurrent import Future

qt4_available = False

try:
    from PyQt4.QtCore import QBuffer, QByteArray, QIODevice
    from PyQt4.QtGui import QPixmap, QApplication
    
    q_app = QApplication([])
    
    qt4_available = True
except:
    from PIL import ImageGrab


REFRESH_INTERVAL = 0.25


class LongPoll(object):
    def __init__(self):
        self.waiters = set()
 
    def wait_for_image(self):
        result_future = Future()
        self.waiters.add(result_future)
        return result_future
 
    def cancel_wait(self, future):
        self.waiters.remove(future)
        future.set_result('')
 
    def new_image(self, image):
        size = len(self.waiters)
        if not size:
            return
            
        print('Serving %d clients' % size)
        for future in self.waiters:
            future.set_result(image)
        self.waiters = set()
        

class MainHandler(RequestHandler):
    def get(self):
        self.render("index.html")
        

class ImgHandler(RequestHandler):
    @coroutine
    def get(self,):
        self.future = self.application.long_poll.wait_for_image()
        image = yield self.future
        if self.request.connection.stream.closed():
            return
        self.set_header('Content-type', 'image/jpg')
        self.write(image)

    def on_connection_close(self):
        self.application.long_poll.cancel_wait(self.future)
        

def make_app():
    return Application(
        [url(r"/", MainHandler),
         url(r"/img", ImgHandler),
         ],
        template_path=os.path.join(os.path.dirname(__file__), "template"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True,
    )

def grab_jpg_image():
    if qt4_available:
        # Using qt4
        img = QPixmap.grabWindow(QApplication.desktop().winId())
        byte_array = QByteArray()
        buffer = QBuffer(byte_array)
        buffer.open(QIODevice.WriteOnly)
        img.save(buffer, 'jpg')
    
        return byte_array.data()
    else:
        # Using PIL/Pillow
        img = ImageGrab.grab()
        buffer = io.BytesIO()
        img.save(buffer, 'jpeg')

        return buffer.getvalue()


@coroutine
def refresh_image(long_poll):
    while True:
        # sleep
        yield Task(IOLoop.current().add_timeout, time.time() + REFRESH_INTERVAL)

        # get image
        image = grab_jpg_image()

        # update waiters if any
        long_poll.new_image(image)

    
def main():
    app = make_app()
    app.long_poll = LongPoll()

    #start getting images
    IOLoop.current().add_callback(refresh_image, app.long_poll)

    app.listen(8001)
    print('http://localhost:8001')
    IOLoop.current().start()
 
 
main()
